from flask import Flask, jsonify
from flask import request
import pandas as pd
import glob as glob
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
import random
import json

app = Flask(__name__)
oe = preprocessing.OrdinalEncoder()
le = preprocessing.LabelEncoder()
homeModel = LogisticRegression()
awayModel = LogisticRegression()


def merge_csv():
    all_files = glob.glob("data/*.csv")
    li = []
    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)
    merged = pd.concat(li, axis=0, ignore_index=True)
    merged['HomeTeam'] = merged['HomeTeam'].str.replace('Ath Madrid','Atlético Madrid')
    merged['HomeTeam'] = merged['HomeTeam'].str.replace('Sociedad', 'Real Sociedad')
    merged['HomeTeam'] = merged['HomeTeam'].str.replace('Celta', 'Celta Vigo')
    merged['HomeTeam'] = merged['HomeTeam'].str.replace('Ath Bilbao', 'Athletic Bilbao')
    merged['HomeTeam'] = merged['HomeTeam'].str.replace('Alaves', 'Alavés')
    merged['AwayTeam'] = merged['AwayTeam'].str.replace('Ath Madrid', 'Atlético Madrid')
    merged['AwayTeam'] = merged['AwayTeam'].str.replace('Sociedad', 'Real Sociedad')
    merged['AwayTeam'] = merged['AwayTeam'].str.replace('Celta', 'Celta Vigo')
    merged['AwayTeam'] = merged['AwayTeam'].str.replace('Ath Bilbao', 'Athletic Bilbao')
    merged['AwayTeam'] = merged['AwayTeam'].str.replace('Alaves', 'Alavés')
    return merged

# Train home team
def train_home():
    merged_data = merge_csv()
    merged_data = merged_data.fillna(0)
    feature_home_cols = ["HomeTeam"]
    series_home_cols = ["FTHG"]
    enc_x = oe.fit_transform(merged_data.loc[:, feature_home_cols])
    X_home_train = enc_x[0:2001]
    Y_home_train = merged_data.loc[0:2000, series_home_cols]
    enc_y = le.fit_transform(Y_home_train)
    homeModel.fit(X_home_train, enc_y)
    test_data = merged_data.drop("FTHG", axis=1)
    test = test_data.loc[:, feature_home_cols]
    X_home_test = oe.fit_transform(test.loc[:, feature_home_cols])
    X_home_test = X_home_test[2001:]
    pred_y = le.inverse_transform(homeModel.predict(X_home_test))
    return 0


# Train away team
def train_away():
    merged_data = merge_csv()
    merged_data = merged_data.fillna(0)
    feature_away_cols = ["AwayTeam"]
    series_away_cols = ["FTAG"]
    enc_x = oe.fit_transform(merged_data.loc[:, feature_away_cols])
    X_away_train = enc_x[0:2001]
    Y_away_train = merged_data.loc[0:2000, series_away_cols]
    enc_y = le.fit_transform(Y_away_train)
    awayModel.fit(X_away_train, enc_y)
    test_data = merged_data.drop("FTAG", axis=1)
    test = test_data.loc[:, feature_away_cols]
    X_away_test = oe.fit_transform(test.loc[:, feature_away_cols])
    X_away_test = X_away_test[2001:]
    pred_y = le.inverse_transform(awayModel.predict(X_away_test))

    return 0


train_home()
train_away()


@app.route('/predict', methods=['POST', 'GET'])
def get_prediction():
    if request.method == 'POST':
        games = json.loads(request.data)
        result = []
        for game in games:
            home = 0
            away = 0
            if game['home'] in oe.categories_[0]:
                enc = oe.transform([[game['home']]])
                prediction = homeModel.predict(enc)
                home = str(prediction[0])
            else:
                home = str(random.randint(0, 4))
            if game['away'] in oe.categories_[0]:
                enc = oe.transform([[game['away']]])
                prediction = awayModel.predict(enc)
                away = str(prediction[0])
            else:
                away = str(random.randint(0, 4))
            result.append(home + "-" + away)
        return jsonify(result)


@app.route('/')
def main():
    return 'Welcome to Winball ML Bot'


if __name__ == '__main__':
    app.run()
